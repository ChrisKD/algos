function minNumberOfCoinsForChange(n, denoms) {
	var numOfCoins = new Array(n + 1).fill(Infinity);
	  
	numOfCoins[0] = 0;
	for (var denom of denoms) {
		for (let amount = 0; amount < numOfCoins.length; amount++) {
			if (denom <= amount) {
				numOfCoins[amount] = Math.min(numOfCoins[amount], numOfCoins[amount - denom] +1); //why adding 1 on top of the final number of coins?				
				
				console.log(numOfCoins);
			}
		}
	}
	return numOfCoins[n] !== Infinity ? numOfCoins[n] : -1;
}


var x = {
	"n": 30,
	"denoms": [1, 5, 10]
}
  
var y = minNumberOfCoinsForChange(x.n, x.denoms);
console.log(y);