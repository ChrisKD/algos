function longestPeak(array) {
	var last = array[0]
	var count = 1;
	var phase = null
	if (array[1] > array[0]) {
		var phase = "ascending";
		count++;
	} else if (array[1] < array[0]) {
		var phase = "noPeakDescending";
	} else if (array[1] === array[0]) {
		var phase = "equal";
	}
	var maxCount = count;
	for (let i = 2; i < array.length; i++) {
		if (phase === "ascending") {
			if (array[i] > array[i-1]) {
				count++;
			} else if (array[i] < array[i-1]) {
				phase = "descending";
				count++;
				count > maxCount ? maxCount = count : null;
			} else {
				phase = "equal";
				count = 1;
			}
		} else if (phase === "descending") {
			if (array[i] > array[i-1]) {
				phase = "ascending";
				count > maxCount ? maxCount = count : null;
				count = 2;
			} else if (array[i] < array[i-1]) {
				count++;
				count > maxCount ? maxCount = count : null;
			} else {
				phase = "equal";
				count > maxCount ? maxCount = count : null;
				count = 1;
			}
		} else {
			if (array[i] > array[i-1]) {
				var phase = "ascending";
				count++;
			} else if (array[1] < array[0]) {
				var phase = "noPeakDescending";
				count = 1;
			} else if (array[1] === array[0]) {
				var phase = "equal";
				count = 1;
			}
		}
	}
	return maxCount >= 3 ? maxCount : 0; 
}

var arrToTest = [1, 2, 3, 3, 4, 0, 10, 6, 5, -1, -3, 2, 3];
var x = longestPeak(arrToTest);
console.log(x)

