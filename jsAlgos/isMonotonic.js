function isMonotonic(array) {
    if (array.length <= 2) return true;
    var upTFdown = undefined;
    for (let i=1; i< array.length; i++) {
        if (upTFdown === null || upTFdown === undefined) {
            if (array[i] > array[i-1]) {
                upTFdown = true;
            } else if (array[i] < array[i-1]) {
                upTFdown = false;
            } else {
                upTFdown = null;
            }
        } else {
            if (upTFdown === true) {
                if (array[i] < array[i-1]) return false;
            } else if (upTFdown === false) {
                if (array[i] > array[i-1]) return false;
            }
        }
    }
    return true;
}

var arr = [-1, -5, -10, -1100, -1100, -1101, -1102, -9001];
var x = isMonotonic(arr);
console.log(x);