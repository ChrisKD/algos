// return the indexes of the two numbers in a list that add up to the target value
// time O(n)
// space O(n)

class Solution {
    twoSum = (nums, target) => {
        var values = {};
        for (let i=0; i < nums.length; i++) {
            var diff = target - nums[i];
            var diffIdxInValues = values[diff];
            if (diffIdxInValues) {
                return [i, diffIdxInValues];
            }
            values[nums[i]] = i;
        }
        return [];
    }
}

var nums = [2,7,11,15]
var target = 18
var solution = new Solution();
var result = solution.twoSum(nums, target);
console.log(result);