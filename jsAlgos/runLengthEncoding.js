function runLengthEncoding(string) {
	var finalStr = "";
	var tallyCurrent = 0;
	  for (let i = 0; i < string.length; i++) {
		  tallyCurrent++;
		  if (string[i] !== string[i+1] || tallyCurrent === 9) {
			  finalStr = `${finalStr}${tallyCurrent}${string[i]}`;
			  tallyCurrent = 0;
		  }
	  }
	  return finalStr;
  }

var myStr = "aaaaAAAAbb"
var x = runLengthEncoding(myStr);
console.log(x)