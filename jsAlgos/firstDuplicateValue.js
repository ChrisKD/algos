function firstDuplicateValue(array) {
	
	var obj = {};
	for (let i=0; i<array.length; i++) {
		var temp = obj[array[i]];
		if (temp === undefined) {
			obj[array[i]] = true;
		} else {
			return array[i];
		}
	}
	return -1;
}

var arr = [2, 1, 5, 2, 3, 3, 4]
console.log(firstDuplicateValue(arr));
