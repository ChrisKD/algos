function spiralTraverse(array) {
	var finalArr = [];  
	var startCol = 0;
	var startRow = 0;
	var endRow = array.length-1;
	
	var longestArrLength = 0;
	for (let i of array) {
		if (i.length > longestArrLength) {
			longestArrLength = i.length;
		}
	}
    let endCol = longestArrLength -1;
	
	while (startCol <= endCol && startRow <= endRow) {
		for (let i=startCol; i <= endCol; i++) {
			finalArr.push(array[startRow][i]);
		}
		
		for (let i=startRow+1; i <= endRow; i++) {
			finalArr.push(array[i][endCol]);
		}
		
		if (startRow !== endRow) {
			for (let i=endCol-1; i >= startCol; i--) {
				finalArr.push(array[endRow][i]);
			}
		}
 
		if (startCol !== endCol) {
			for (let i=endRow-1; i > startRow; i--) {
				finalArr.push(array[i][startCol]);
			}
		}
		startRow++;
		endRow--;
		startCol++;
		endCol--;
	}
	return finalArr;
}

var arrays = [
    [1,2,3,4],
    [12,13,14,5],
    [11,16,15,6],
    [10,9,8,7]
]

var x = spiralTraverse(arrays);
console.log(x);