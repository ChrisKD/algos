#return the indexes of the two numbers in a list that add up to the target value
#time O(n)
#space O(n)
class Solution(object):
    def twoSum(self, nums, target):
        values = {}
        for i, a in enumerate(nums):
            diff = target - a
            if diff in values:
                return [i, values[diff]]
            values[a] = i
        return []


nums = [2,7,11,15]
target = 18
result = Solution().twoSum(nums, target)
print(result)