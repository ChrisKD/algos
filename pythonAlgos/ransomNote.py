# Time O(n+m), Space O(1)
def ransomNote(arr, word):
    myDict = {}
    for letter in arr:
        if letter in myDict:
            myDict[letter] = myDict[letter] + 1
        else:
            myDict[letter] = 1
    for eachChar in word:
        if eachChar not in myDict or myDict[eachChar] < 1:
            return False
        myDict[eachChar] = myDict[eachChar] - 1
    return True

arr = ['a','b','c','d','e','f']

# print(ransomNote(arr, 'bed'))
# print(ransomNote(arr, 'bedd'))
# print(ransomNote(arr, 'cat'))
print(ransomNote(arr, 'zyxz'))

# from collections import defaultdict

# class Solution(object):
#   def canSpell(self, magazine, note):
#     letters = defaultdict(int)
#     for c in magazine:
#       letters[c] += 1l

#     for c in note:
#       if letters[c] <= 0:
#         return False      
#       letters[c] -= 1

#     return True

# print(Solution().canSpell(['a', 'b', 'c', 'd', 'e', 'f'], 'bed'))
# # True

# print(Solution().canSpell(['a', 'b', 'c', 'd', 'e', 'f'], 'cat'))
# # False

# print(Solution().canSpell(['a', 'b', 'c', 'd', 'e', 'f'], 'cat'))
# # False



1