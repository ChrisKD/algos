# Time O(n), Space O(n)
class Node(object):
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

class Solution(object):
    def _isValidBstHelper(self, n, low, high):
        if not n:
            return True
        if ((n.val > low and n.val < high) and
            self._isValidBstHelper(n.left, low, n.val ) and 
            self._isValidBstHelper(n.right, n.val, high)):
            return True
        return False

    def isValidBst(self, n):
        return self._isValidBstHelper(n, float('-inf'), float('inf'))


node = Node(5)
node.left = Node(4)
node.right = Node(7)
node.right.left = Node(6)
node.right.right = Node(8)

sol = Solution().isValidBst(node)
print(sol)
#True

node = Node(5)
node.left = Node(4)
node.right = Node(7)
node.right.left = Node(2)
node.right.right = Node(8)
print(Solution().isValidBst(node))
# False